" Vim color file
" Maintainer:	Hans Fugal <hans@fugal.net>
" Last Change:	$Date: 2003/07/24 00:57:11 $
" Last Change:	$Date: 2003/07/24 00:57:11 $
" URL:		http://hans.fugal.net/vim/colors/desert.vim
" Version:	$Id: desert.vim,v 1.7 2003/07/24 00:57:11 fugalh Exp $

" cool help screens
" :he group-name
" :he highlight-groups
" :he cterm-colors

set background=dark
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
    syntax reset
    endif
endif
let g:colors_name="blue-submarine"

hi Normal	guifg=azure guibg=#32424C

" highlight groups
hi Cursor	    guibg=indianred guifg=khaki
hi CursorLine   guibg=#23465B
hi VertSplit	gui=none guifg=seagreen
hi Folded	    guibg=#283B46 guifg=gold
hi FoldColumn	guibg=#283B46 guifg=tan
hi IncSearch	guifg=slategrey guibg=khaki
hi LineNr       guifg=skyblue4
hi ModeMsg	    guifg=goldenrod
hi MoreMsg	    guifg=SeaGreen
hi NonText	    guifg=LightBlue guibg=#32424C
hi Question	    guifg=springgreen
hi Search	    guibg=yellow2 guifg=grey20
hi SpecialKey	guifg=yellowgreen
hi StatusLine	guibg=#23465B gui=none
hi StatusLineNC	guibg=#23465B guifg=yellow2 gui=none
hi Title	    guifg=yellowgreen
hi Visual	    gui=none guifg=khaki guibg=olivedrab
hi WarningMsg   guifg=salmon
hi MatchParen   guibg=lightseagreen guifg=#dfffdf
hi Pmenu        guibg=olivedrab guifg=khaki gui=bold
hi PmenuSel     guifg=olivedrab guibg=khaki gui=bold
hi WildMenu     guibg=olivedrab guifg=khaki gui=bold
hi SignColumn   guibg=#23465B

" syntax highlighting groups
hi Statement    gui=bold guifg=aquamarine2 "turquoise3 "chartreuse3 "lightseagreen" aquamarine2 "chartreuse3 "cyan2
hi Type		    gui=bold guifg=turquoise3 "aquamarine3
hi Identifier	gui=bold guifg=yellowgreen
hi Constant     guifg=cyan
hi String       guifg=olivedrab3
hi Comment      guifg=slategray
hi PreProc	    guifg=SeaGreen
hi Special	    guifg=navajowhite
hi Ignore	    guifg=grey40
hi Todo		    guifg=orangered guibg=yellow2

