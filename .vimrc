set nocompatible
set ts=4           "tab = 4 spaces
set autoindent     "auto ident filetype
set softtabstop=4 shiftwidth=4 expandtab   "wstawia 4 spacje zamiast taba
set nowrap         "noline wrape
set guioptions+=b  "botom scrolbar
set guioptions-=T  "remove iconbar
set number         "show line number
set history=50     "length of history
set incsearch      "increment search
set showcmd        "show typing comand
set cindent shiftwidth=4   "automatyczne wcięcia o głębokości 4
set cursorline     "hilighting current line
"set nobackup      "no backup files
"set nowritebackup "only in case you don't want a backup file while editing
set wildmenu       "shows possible command completion
set noswapfile     "no swap files
set mouse=a        "enable mouse in vim
set wmh=0          "minimazie split to zero
set hlsearch       "\l show trailing space
set listchars+=trail:.
set autoread       "auto reload changed files
"highlight SpecialKey term=standout ctermbg=red guibg=red
set fillchars+=vert:\   "get rid of character in vsplit separator
set guicursor+=n-v-c:blinkon0-block "disable bursor blinking
set encoding=utf-8
set termguicolors

"hotkeys from windows"
source $VIMRUNTIME/mswin.vim
behave mswin
set keymodel-=stopsel "arrows work in visual - aftew behave mswin

"colorscheme desert "set colorscheme
"setings for popup menu for desert colorscheme
"hl Pmenu guibg=deepSkyBlue4 gui=bold
colorscheme blue-submarine "set colorscheme

syntax enable      "enable syntax

"pathogen
let g:pathogen_disabled = [] "disabled plugins
"call add(g:pathogen_disabled, 'YouCompleteMe')
"call add(g:pathogen_disabled, 'AutoComplPop')
call pathogen#infect() "load plugins from ~/.vim/bundle
call pathogen#helptags()
filetype plugin on

"CplaneTags for tags generated with tagsGen
let g:CPlaneTagsComponents = ["rrom", "enbc", "cellc", "pit", "uec", "tupc", "mcec"]

"Airline
set laststatus=2   "airline always on
let g:airline_theme='solarized' " 'bubblegum'
let g:airline_section_error = airline#section#create_right(['%{g:asyncrun_status}'])

"CtrlP
let g:ctrlp_regexp = 1 "regexp serch by default
let g:ctrlp_map = '<c-f>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_extensions = ['tag']
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_root_markers = ['.config']
let g:ctrlp_max_depth = 40
let g:ctrlp_max_files = 0 "0 - nolimit
let g:ctrlp_clear_cache_on_exit = 0 "dont clear cache
let g:ctrlp_lazy_update = 1
"let g:ctrlp_custom_ignore = {
"    \ 'dir':  '\v[\/]\.(git|hg|svn)$',
"    \ 'file': '\v\.(exe|so|dll)$',
"    \ 'link': 'SOME_BAD_SYMBOLIC_LINKS',
"    \ }

"YouCompleteMe
let g:ycm_filetype_whitelist = {'cpp' : 1, 'js' : 1, 'python' : 1}
"let g:ycm_server_keep_logfiles = 1
"let g:ycm_keep_logfiles = 1
let g:ycm_confirm_extra_conf = 0 "dont ask to reload .ycm_extra_conf.py
let g:ycm_global_ycm_extra_conf = "~/.vim/bundle/YouCompleteMe/third_party/ycmd/examples/.ycm_extra_conf.py"
"let g:ycm_path_to_python_interpreter = '/usr/bin/python'
"let g:loaded_youcompleteme = 1 "disable ycm

"NERDTree
let NERDTreeIgnore = ['\.pyc$']
let NERDTreeShowBookmarks=1
nmap <silent> <F5> :NERDTreeToggle<CR>

"file extension ignore in search
:set wildignore=.svn,CVS,.git,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.png,*.xpm,*.gif,*.pyc,*.svn-base
command RemoveTrash %s/^.*FCT-\d011-\d\+-//
command TimeSort sort /<\S*T\S*>/ r
command MakeThis make '%:r' CXXFLAGS+="-pthread -std=c++14 -Wall" | ! ./'%:r'

"auto open quickfix when asynccomand is started
autocmd User AsyncRunStart call asyncrun#quickfix_toggle(8, 1)

"auto remove trailing whitespaces
autocmd BufWritePre * :%s/\s\+$//e

" OmniCppComplete
let OmniCpp_SelectFirstItem = 2 " select first item (but don't insert)
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 0 " autocomplete after .
let OmniCpp_MayCompleteArrow = 0 " autocomplete after ->
let OmniCpp_MayCompleteScope = 0 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
set completeopt=menuone,menu,longest
"OmniCppComplete
inoremap <silent> <C-space><C-space> <C-X><C-O>
"inoremap <silent> <C-space> <C-X><C-P>


"Vim-Signature
"set highlight for SignatureMark
let g:SignatureMarkTextHL = "VimSignatureMark"
hi VimSignatureMark gui=bold guifg=orchid
"let g:SignatureMarkLineHL = "VimSignatureMarkLine"
"hi VimSignatureMarkLine guibg=#283B46

"set highlight for SignatureMarker
let g:SignatureMarkerTextHL = "VimSignatureMarker"
hi VimSignatureMarker gui=bold guifg=green
"let g:SignatureMarkerLineHL = "VimSignatureMarkerLine"
"hi VimSignatureMarkerLine guibg=#283B46


"foldinf
" zi - toggle folding
" zM - zwija wszystko
" zm - zwija jeden poziom
" zR - rozwija wszystko
" zr - rozwija jeden poziom
" zC - zwija całkowicie cały marker
" zc - zwija jeden marker o jeden poziom
" zO - rozwija cały marker
" zo - rozwija jeden marker o jeden poziom
set nofoldenable       "foldowanie off
set foldcolumn=2       "dodaje kolumne do foldowania
set foldmethod=indent  "zwijajne wedle wciec
autocmd FileType c,cpp,h,hpp :set foldmethod=syntax "zwijanie lini wedle syntaxu dla c++
autocmd FileType ttcn :set colorcolumn=130
"##################################################"
"cplane
map <F3> :call CPlaneJumpHeaderSource()<CR>

"######### skróty klawiszowe #####################"
nmap <silent> <F6> :split<CR>
nmap <silent> <F4> :TagbarToggle<CR>
nmap <silent> <F7> :tabnew<CR>
nmap <silent> <F8> :vsplit<CR>
" Execute file being edited with <Shift> + e:
map <buffer> <S-e> :w<CR>:!/usr/bin/env python % <CR>

" usun biale znaki z konca linii
map <F12> :%s/\s*$//g<CR>

"rm syslog trashes
"map <F11> :%s/^.*-\d-\(\h\+exe\) /\1 /<CR>
map <F11> :%s/^[0-9]\+\s\+[0-9.]\+\s[0-9.:]\+\s\+\[[0-9.]\+\]\s\+[0-9a-f]\+\s\+//<CR>

" przeczanie zakładek pod ctrl+tab"
map <silent> <C-Tab> :tabnext<CR>
inoremap <silent> <C-Tab> <esc>:tabnext<CR>a

" przełączanie buforót pod tab
nmap <silent> <Tab> :bNext<CR>

"\n to turn off search highlighting
nmap <silent> <leader>n :silent :nohlsearch<CR>
"\l to toggle visible whitespace
nmap <silent> <leader>l :set list!<CR>

"##################################################"
" resize current buffer by +/- 5
nnoremap <silent><C-LEFT>  :vertical resize -5<cr>
nnoremap <silent><C-DOWN>  :resize +5<cr>
nnoremap <silent><C-UP>    :resize -5<cr>
nnoremap <silent><C-RIGHT> :vertical resize +5<cr>
