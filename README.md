# README #

This repo contains my .vim folder and .vimrc
It has plugins I use in daily work. Some plugins are mine and belong to repository other are submodules.

### How do I get set up? ###

* git clone https://bitbucket.org/akrzyz/vim-stuffs.git .vim
* git submodule init
* git submodule update


Now repo is set up and you can manage your .vimrc or simply make symlink to .vim/.vimrc if you wish to use mine.

### How to update plugins? ###

* git submodule update --remote 